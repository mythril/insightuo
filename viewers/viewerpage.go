package viewers

import (
	"bitbucket.org/mythril/insightuo/loaders"
	"fyne.io/fyne/v2"
)

// ViewerPage is used to generalize over a set of
// widgets an allow them to be dynamically
// added to the applications main notebook
type ViewerPage interface {
	GetTabLabel() string
	GetPageWidget() fyne.CanvasObject
}

var ViewerBuilders = map[loaders.UOResourceType]func(*loaders.Loader) ViewerPage{
	loaders.ArtResourceType: func(l *loaders.Loader) ViewerPage {
		return NewArtViewer(l)
	},
}
