package viewers

import (
	"fmt"
	"log"

	"bitbucket.org/mythril/insightuo/loaders"
	"bitbucket.org/mythril/insightuo/uiutils"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

// ArtViewer is for managing the UI for viewing UO's art files
type ArtViewer struct {
	page   fyne.CanvasObject
	tab    string
	loader loaders.ArtLoader
}

func IndexedTable(loader loaders.Loader, onSelected func(loaders.Art)) (*fyne.Container, *widget.Table) {
	index := loader.GetIndexedLoader()
	_, tableIndex := index.GetValidIndexEntries()
	artLoader, ok := loader.(*loaders.ArtLoader)
	if ok == false {
		log.Fatal("Failed to acquire a handle to an ArtLoader")
	}
	width := func() int {
		return 10
	}
	lookup := func(row, col int) loaders.Art {
		art := *artLoader.Get(tableIndex[row*width()+col].ID)
		return art
	}
	table := widget.NewTable(
		func() (int, int) {
			return index.CountValidIndexes() / width(), width()
		},
		func() fyne.CanvasObject {
			image := &canvas.Image{}
			image.SetMinSize(fyne.NewSize(44, 44))
			return image
		},
		func(tci widget.TableCellID, iw fyne.CanvasObject) {
			image := iw.(*canvas.Image)
			image.Image = lookup(tci.Row, tci.Col).Sprite
			image.Refresh()
		},
	)
	table.OnSelected = func(id widget.TableCellID) {
		onSelected(lookup(id.Row, id.Col))
	}
	layout := uiutils.NewWithResizeHandler(layout.NewMaxLayout(), func(size fyne.Size) {
		width = func() int {
			if size.Width >= 53 {
				return int(size.Width / 53)
			}
			return 1
		}
		table.Refresh()
	})
	wrapper := fyne.NewContainerWithLayout(layout)
	wrapper.Add(table)
	return wrapper, table
}

// NewArtViewer constructor for ArtViewer
func NewArtViewer(l *loaders.Loader) *ArtViewer {
	loader, ok := (*l).(*loaders.ArtLoader)
	if ok != true {
		log.Fatal("Bad loader passed to NewArtViewer")
	}
	artTabs := container.NewAppTabs()
	hexLabel := widget.NewLabel("ID (hex):")
	hexID := widget.NewEntry()
	hexID.Disable()
	decLabel := widget.NewLabel("ID (dec):")
	decID := widget.NewEntry()
	decID.Disable()
	showbox := container.NewGridWithColumns(
		2,
		hexLabel,
		hexID,
		decLabel,
		decID,
	)

	artCard := widget.NewCard("Landscape tiles", "art.mul index < 0x4000", showbox)
	image := canvas.NewImageFromImage(loader.Get(0).Sprite)
	image.FillMode = canvas.ImageFillContain
	image.ScaleMode = canvas.ImageScalePixels
	artCard.SetImage(image)
	tablewrapper, table := IndexedTable(loader, func(art loaders.Art) {
		image.Image = art.Sprite
		image.Refresh()
		artCard.SetTitle(fmt.Sprintf("Landscape tile: 0x%X (%v)", art.Entry.ID, art.Entry.ID))
		hexID.SetText(fmt.Sprintf("0x%X", art.Entry.ID))
		decID.SetText(fmt.Sprintf("%v", art.Entry.ID))
	})
	table.Select(widget.TableCellID{Row: 0, Col: 0})
	landscapeTilesTab := container.NewTabItem(
		"Landscape Tiles",
		tablewrapper,
	)
	artTabs.Append(landscapeTilesTab)
	artTabs.SelectTabIndex(0)
	return &ArtViewer{
		page:   container.NewHSplit(artTabs, artCard),
		tab:    "Art Viewer",
		loader: *loader,
	}
}

func (av *ArtViewer) GetTabLabel() string {
	return (*av).tab
}

func (av *ArtViewer) GetPageWidget() fyne.CanvasObject {
	return (*av).page
}
