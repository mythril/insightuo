package uiutils

import (
	// "unsafe"

	"image"
	"image/png"
	"log"
	"os"
	// "github.com/veandco/go-sdl2/sdl"
)

// Image is intended to be an underlying format that can be converted to
// gotk3 and go-sdl2 images respectively, no gaurantees are made about
// updating the resulting pixbuf/surface when the data is changed

type Image struct {
	*image.RGBA
}

// NewImage Constructs an image with an empty buffer already allocated
func NewImage(width, height uint32) Image {
	return Image{image.NewRGBA(image.Rect(0, 0, int(width), int(height)))}
}

// FillWith sets every pixel in the image to the specified rgba value
func (i *Image) FillWith(r, g, b, a byte) {
	for j := 0; j < len(i.Pix); j += 4 {
		i.Pix[j] = r
		i.Pix[j+1] = g
		i.Pix[j+2] = b
		i.Pix[j+3] = a
	}
}

func (i *Image) PngOut(filename string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	if err := png.Encode(f, i); err != nil {
		f.Close()
		log.Fatal(err)
	}
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
