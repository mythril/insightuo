package uiutils

import (
	"fyne.io/fyne/v2"
)

type WithResizeHandler struct {
	wrapped  fyne.Layout
	OnResize func(fyne.Size)
}

func NewWithResizeHandler(layout fyne.Layout, onResize func(fyne.Size)) *WithResizeHandler {
	return &WithResizeHandler{
		wrapped:  layout,
		OnResize: onResize,
	}
}

func (wrh *WithResizeHandler) Layout(objects []fyne.CanvasObject, containerSize fyne.Size) {
	wrh.OnResize(containerSize)
	wrh.wrapped.Layout(objects, containerSize)
}

func (wrh *WithResizeHandler) MinSize(objects []fyne.CanvasObject) fyne.Size {
	return wrh.wrapped.MinSize(objects)
}
