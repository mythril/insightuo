package insightuo

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/mythril/insightuo/viewers"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/widget"
	"gopkg.in/yaml.v2"
)

type configuration struct {
	UORoot string `yaml:"uoRoot"`
}

func loadConfig(filename string) (*configuration, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	ret := configuration{}
	err = yaml.Unmarshal(data, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

func saveConfig(config configuration, filename string) error {
	data, err := yaml.Marshal(&config)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func Init() {
	configFn := "insightuo.yaml"
	config := configuration{UORoot: ""}
	if _, err := os.Stat(configFn); err == nil {
		loadedConfig, err := loadConfig("insightuo.yaml")
		if err != nil {
			log.Printf("Unable to load insightuo.yaml: %s", err)
		}
		if loadedConfig != nil {
			config = *loadedConfig
		}
	}

	application := app.New()
	fwin := application.NewWindow("InsightUO")
	fwin.Resize(fyne.NewSize(800, 600))

	fspOuterContent := container.New(layout.NewCenterLayout())
	fspInnerContent := container.New(layout.NewVBoxLayout())
	fspOuterContent.Add(fspInnerContent)
	fileSetupPage := container.NewTabItem(
		"File Locations",
		fspOuterContent,
	)

	maintabs := container.NewAppTabs(fileSetupPage)
	maintabs.SetTabLocation(container.TabLocationLeading)

	notebookPages := make([]*container.TabItem, 0)
	addNotebookPage := func(page viewers.ViewerPage) {
		tabItem := container.NewTabItem(page.GetTabLabel(), page.GetPageWidget())
		maintabs.Append(tabItem)
		notebookPages = append(notebookPages, tabItem)
	}
	clearNotebookPages := func() {
		for _, v := range notebookPages {
			maintabs.Remove(v)
		}
		notebookPages = make([]*container.TabItem, 0)
	}
	loadViewers := func(fname string) {
		detectedMedia, err := scanDirForMedia(fname)
		if err != nil {
			// @TODO show the user this error
			fmt.Println("Scan came up empty")
			fmt.Println(err)
		}
		clearNotebookPages()
		pageBuilders := getPageBuildersFromMedia(detectedMedia)
		for _, builder := range pageBuilders {
			page := builder()
			addNotebookPage(page)
		}
	}
	selectedFolder := widget.NewLabel("<None set>")
	if config.UORoot != "" {
		loadViewers(config.UORoot)
		selectedFolder.SetText(config.UORoot)
	}

	fo := dialog.NewFolderOpen(func(lu fyne.ListableURI, e error) {
		fname := lu.Path()
		loadViewers(fname)
		config.UORoot = fname
		selectedFolder.SetText(config.UORoot)
		err := saveConfig(config, configFn)
		if err != nil {
			// @TODO show the user this error
			fmt.Println(err)
		}
	}, fwin)
	folderChooserButton := widget.NewButton("Locate UO Root", func() {
		fo.Show()
	})
	if config.UORoot != "" {
		fileURI := storage.NewFileURI(config.UORoot)
		lister, err := storage.ListerForURI(fileURI)
		if err != nil {
			log.Fatal(err)
		}
		fo.SetLocation(lister)
	}

	fwin.CenterOnScreen()

	fspInnerContent.Add(folderChooserButton)
	fspInnerContent.Add(selectedFolder)
	fwin.SetContent(maintabs)
	fwin.ShowAndRun()
}
