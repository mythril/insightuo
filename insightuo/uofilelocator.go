package insightuo

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/mythril/insightuo/loaders"
	"bitbucket.org/mythril/insightuo/viewers"
)

var indexToType = map[string]loaders.UOResourceType{
	"artidx.mul":   loaders.ArtResourceType,
	"gumpidx.mul":  loaders.GumpResourceType,
	"texidx.mul":   loaders.TextureResourceType,
	"soundidx.mul": loaders.SoundResourceType,
	"multi.idx":    loaders.MultiResourceType,
	"anim.idx":     loaders.AnimationResourceType,
	"skills.idx":   loaders.SkillResourceType,
}

var mediaToIndex = map[string]string{
	"art.mul":     "artidx.mul",
	"gumpart.mul": "gumpidx.mul",
	"texmaps.mul": "texidx.mul",
	"sound.mul":   "soundidx.mul",
	"multi.mul":   "multi.idx",
	"anim.mul":    "anim.idx",
	"skills.mul":  "skills.idx",
}

var typeToIndex = map[loaders.UOResourceType]string{
	loaders.ArtResourceType:       "artidx.mul",
	loaders.GumpResourceType:      "gumpidx.mul",
	loaders.TextureResourceType:   "texidx.mul",
	loaders.SoundResourceType:     "soundidx.mul",
	loaders.MultiResourceType:     "multi.idx",
	loaders.AnimationResourceType: "anim.idx",
	loaders.SkillResourceType:     "skills.idx",
}

var mediaFileToType = map[string]loaders.UOResourceType{
	"art.mul":     loaders.ArtResourceType,
	"gumpart.mul": loaders.GumpResourceType,
	"texmaps.mul": loaders.TextureResourceType,
	"sound.mul":   loaders.SoundResourceType,
	"multi.mul":   loaders.MultiResourceType,
	"anim.mul":    loaders.AnimationResourceType,
	"skills.mul":  loaders.SkillResourceType,
	"fonts.mul":   loaders.FontResourceType,
	"hues.mul":    loaders.HueResourceType,
	"speech.mul":  loaders.SpeechResourceType,
}

var typeToMediaFile = map[loaders.UOResourceType]string{
	loaders.ArtResourceType:       "art.mul",
	loaders.GumpResourceType:      "gumpart.mul",
	loaders.TextureResourceType:   "texmaps.mul",
	loaders.SoundResourceType:     "sound.mul",
	loaders.MultiResourceType:     "multi.mul",
	loaders.AnimationResourceType: "anim.mul",
	loaders.SkillResourceType:     "skills.mul",
	loaders.FontResourceType:      "fonts.mul",
	loaders.HueResourceType:       "hues.mul",
	loaders.SpeechResourceType:    "speech.mul",
}

func fileIsUORelated(basename string) bool {
	normalizedName := strings.ToLower(basename)
	if _, exists := mediaFileToType[normalizedName]; exists != false {
		return true
	}
	if _, exists := indexToType[normalizedName]; exists != false {
		return true
	}
	return false
}

func canonicalizePath(path string) string {
	abs, err := filepath.Abs(path)
	if err != nil {
		fmt.Println(err)
	}
	result, err := filepath.EvalSymlinks(abs)
	if err != nil {
		fmt.Println(err)
	}
	return result
}

func scanDirForMedia(filepath string) (map[string]string, error) {
	dir, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	entries, err := dir.Readdir(0)
	if err != nil {
		return nil, err
	}
	var media = map[string]string{}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		if fileIsUORelated(entry.Name()) {
			var sb strings.Builder
			sb.WriteString(filepath)
			sb.WriteRune(os.PathSeparator)
			sb.WriteString(entry.Name())
			var path = canonicalizePath(sb.String())
			var normalizedBasename = strings.ToLower(entry.Name())
			media[normalizedBasename] = path
		}
	}
	return media, nil
}

func getPageBuildersFromMedia(media map[string]string) map[loaders.UOResourceType]func() viewers.ViewerPage {
	var pageBuilders = map[loaders.UOResourceType]func() viewers.ViewerPage{}
	for mediaBaseName, mediaType := range mediaFileToType {
		detectedMedia, ok := media[mediaBaseName]
		if ok != true {
			continue
		}
		viewerBuilder, ok := viewers.ViewerBuilders[mediaType]
		if ok == false {
			// @TODO report this
			// No viewer builder found
			continue
		}
		indexBaseName, indexShouldExist := mediaToIndex[mediaBaseName]
		if indexShouldExist == true {
			detectedIndex, ok := media[indexBaseName]
			if ok != true {
				// @TODO report that
				// an index file is missing
			} else {
				loaderBuilder, ok := loaders.IndexedLoaders[mediaType]
				if ok == false {
					// @TODO report this
					// no loader type found
					continue
				}
				pageBuilders[mediaType] = func() viewers.ViewerPage {
					iloader := *loaders.NewIndexedLoader(mediaType, detectedIndex)
					loader := loaderBuilder(detectedMedia, iloader)
					viewer := viewerBuilder(&loader)
					return viewer
				}
			}
		} else {
			loaderBuilder, ok := loaders.SimpleLoaders[mediaType]
			if ok == false {
				// @TODO report this
				// no loader type found
				continue
			}
			pageBuilders[mediaType] = func() viewers.ViewerPage {
				loader := loaderBuilder(detectedMedia)
				viewer := viewerBuilder(&loader)
				return viewer
			}
		}
	}
	return pageBuilders
}
