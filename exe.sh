#!/bin/bash
go clean
go build -o insightuo.exe -ldflags -H=windowsgui *.go || exit 1
rm -fr build-windows
mkdir build-windows
mv insightuo.exe build-windows
pushd build-windows

zip -q -r ../insightuo.zip ./

popd

