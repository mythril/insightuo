package loaders

// UOResourceType integer to identify resource type
type UOResourceType int

// Media file categories associated with UO
const (
	ArtResourceType UOResourceType = iota
	GumpResourceType
	TextureResourceType
	SoundResourceType
	MultiResourceType
	AnimationResourceType
	SkillResourceType
	FontResourceType
	HueResourceType
	SpeechResourceType
)

type Loader interface {
	// Get(uint32) *UOResource
	GetResourceType() UOResourceType
	GetIndexedLoader() *IndexedLoader
}

var SimpleLoaders = map[UOResourceType]func(string) Loader{}
var IndexedLoaders = map[UOResourceType]func(string, IndexedLoader) Loader{
	ArtResourceType: func(artFile string, index IndexedLoader) Loader {
		return NewArtLoader(artFile, index)
	},
}
