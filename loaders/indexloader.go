package loaders

import (
	"encoding/binary"
	"log"
	"os"

	"github.com/edsrzf/mmap-go"
)

type IndexEntry struct {
	ID     uint32
	offset uint32
	length uint32
	Extra  uint32
}

type IndexedLoader struct {
	Loader
	index        mmap.MMap
	cache        map[uint32]IndexEntry
	orderedCache []IndexEntry
	entryCount   int64
	fullyCached  bool
}

func (il *IndexedLoader) CountValidIndexes() int {
	if il.fullyCached != true {
		il.CacheEntireIndex()
	}
	return len(il.cache)
}

func (il *IndexedLoader) CacheEntireIndex() {
	if il.fullyCached == true {
		return
	}
	il.orderedCache = make([]IndexEntry, 0)
	for i := 0; i < int(il.entryCount); i++ {
		if i > 0x40_00 {
			break
		}
		entry := il.GetIndexEntry(uint32(i))
		if entry.IsValid() {
			il.orderedCache = append(il.orderedCache, entry)
		}
	}
	il.fullyCached = true
}

func (il *IndexedLoader) GetValidIndexEntries() (map[uint32]IndexEntry, []IndexEntry) {
	if il.fullyCached != true {
		il.CacheEntireIndex()
	}
	return il.cache, il.orderedCache
}

func (il *IndexedLoader) GetIndexEntry(id uint32) IndexEntry {
	if cached, found := il.cache[id]; found == true {
		return cached
	}

	ie := IndexEntry{}
	startPos := id * EntryWidth
	ie.ID = id
	ie.offset = binary.LittleEndian.Uint32(il.index[startPos : startPos+4])
	startPos += 4
	ie.length = binary.LittleEndian.Uint32(il.index[startPos : startPos+4])
	startPos += 4
	ie.Extra = binary.LittleEndian.Uint32(il.index[startPos : startPos+4])

	if ie.IsValid() {
		il.cache[id] = ie
	}

	return ie
}

const EntryWidth = 12

func (ie IndexEntry) IsValid() bool {
	return ie.offset != 0xff_ff_ff_ff
}

func NewIndexedLoader(resource UOResourceType, indexFile string) *IndexedLoader {
	openedIndex, err := os.Open(indexFile)
	if err != nil {
		log.Fatal(err)
	}
	istat, err := openedIndex.Stat()
	if err != nil {
		log.Fatal(err)
	}
	index, err := mmap.Map(openedIndex, mmap.RDONLY, 0)
	if err != nil {
		log.Fatal(err)
	}
	entryCount := istat.Size() / EntryWidth
	return &IndexedLoader{
		index:       index,
		entryCount:  entryCount,
		cache:       make(map[uint32]IndexEntry),
		fullyCached: false,
	}
}
