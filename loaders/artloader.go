package loaders

import (
	"encoding/binary"
	"image/color"
	"log"
	"os"

	"bitbucket.org/mythril/insightuo/uiutils"
	"github.com/edsrzf/mmap-go"
)

type ArtLoader struct {
	Loader
	resource UOResourceType
	index    IndexedLoader
	media    mmap.MMap
}

func NewArtLoader(mediaFile string, index IndexedLoader) *ArtLoader {
	openedMedia, err := os.Open(mediaFile)
	if err != nil {
		log.Fatal(err)
	}
	media, err := mmap.Map(openedMedia, mmap.RDONLY, 0)
	if err != nil {
		log.Fatal(err)
	}
	return &ArtLoader{
		resource: ArtResourceType,
		index:    index,
		media:    media,
	}
}

func (a ArtLoader) GetIndexedLoader() *IndexedLoader {
	return &a.index
}

func (a *ArtLoader) GetResourceType() UOResourceType {
	return a.resource
}

type ArtEntry struct {
	IndexEntry
}

type Art struct {
	Entry  ArtEntry
	Sprite uiutils.Image
	header uint32
}

func rgbauint16toRGBA(color16 uint16) color.RGBA {
	a := byte(255)
	// & 0x1f : preserves only the 5 least significant bits
	// * 255 / 31 : scales a 5 bit value to an 8 bit value
	r := byte(((color16 >> 10) & 0x1f) * 255 / 31)
	g := byte(((color16 >> 5) & 0x1f) * 255 / 31)
	b := byte((color16 & 0x1f) * 255 / 31)
	return color.RGBA{R: r, G: g, B: b, A: a}
}

func (al *ArtLoader) getTile(id uint32) *Art {
	ae := ArtEntry{al.index.GetIndexEntry(id)}
	if ae.IsValid() == false {
		return nil
	}
	image := uiutils.NewImage(44, 44)

	image.FillWith(0, 0, 0, 0)
	rawData := al.media[int(ae.offset) : int(ae.offset)+int(ae.length)]

	var nx, dstpos, width int
	srcpos := 0
	width = 2
	for y := 0; y < 22; y++ {
		for x := 0; x < width; x++ {
			color16 := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
			rgba := rgbauint16toRGBA(color16)
			srcpos += 2
			nx = x + (44-width)/2
			dstpos = (y*4*44 + nx*4)
			image.Pix[dstpos+0] = rgba.R
			image.Pix[dstpos+1] = rgba.G
			image.Pix[dstpos+2] = rgba.B
			image.Pix[dstpos+3] = rgba.A
		}
		width += 2
	}

	width = 44

	for y := 22; y < 44; y++ {
		for x := 0; x < width; x++ {
			color16 := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
			srcpos += 2
			rgba := rgbauint16toRGBA(color16)
			nx = x + (44-width)/2
			dstpos = y*44*4 + nx*4
			image.Pix[dstpos+0] = rgba.R
			image.Pix[dstpos+1] = rgba.G
			image.Pix[dstpos+2] = rgba.B
			image.Pix[dstpos+3] = rgba.A
		}
		width -= 2
	}

	return &Art{
		Entry:  ae,
		Sprite: image,
	}
}

func (al *ArtLoader) getSprite(id uint32) *Art {
	ae := ArtEntry{al.index.GetIndexEntry(id)}
	if ae.IsValid() == false {
		return nil
	}
	art := Art{}

	rawData := al.media[int(ae.offset) : int(ae.offset)+int(ae.length)]
	srcpos := 0
	art.header = binary.LittleEndian.Uint32(rawData[srcpos : srcpos+4])
	srcpos += 4
	width := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
	srcpos += 2
	height := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
	srcpos += 2

	lineStartOffsets := make([]uint16, height)
	for i := 0; i < int(height); i++ {
		lineStartOffsets[i] = binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
		srcpos += 2
	}

	image := uiutils.NewImage(uint32(width), uint32(height))
	image.FillWith(0, 0, 0, 0)

	dataStart := srcpos
	y := 0
	x := 0
	srcpos = dataStart + int(lineStartOffsets[y])*2
	for y < int(height) {
		xOffset := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
		srcpos += 2
		length := binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2])
		srcpos += 2
		if xOffset+length > 2048 {
			break
		} else if xOffset+length != 0 {
			x += int(xOffset)
			for run := length; run > 0; run-- {
				rgba := rgbauint16toRGBA(binary.LittleEndian.Uint16(rawData[srcpos : srcpos+2]))
				srcpos += 2
				image.SetRGBA(x, y, rgba)
			}
			x += int(length)
		} else {
			x = 0
			y++
			srcpos = dataStart + int(lineStartOffsets[y])*2
		}
	}

	art.Sprite = image

	return &art
}

func (a *ArtLoader) getLegacyArt(id uint32) *Art {
	log.Fatal("not implemented")
	return nil
}

func (a *ArtLoader) Get(id uint32) *Art {
	if id < 0x4000 {
		return a.getTile(id)
	}
	if id < 0x8000 {
		return a.getSprite(id)
	}
	return a.getLegacyArt(id)
}
